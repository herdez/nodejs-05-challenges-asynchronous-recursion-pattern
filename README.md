# NodeJS Challenges - CommonJS Pattern & Asynchronous Recursion

## Objectives:

- To learn about `Asynchronous Recursion with Callbacks`.
- To learn about `CommonJS Module Pattern`.
- To learn for seeking information, reading documentation and checking out topics 
  about `NodeJS`.


### Theory & Documentation

# Asynchronous Recursion with Callbacks

```javascript
const getSentenceFragment = (offset = 0) => {
  const pageSize = 3
  const sentence = [...'hello world']
  return {
    data: sentence.slice(offset, offset + pageSize),
    nextPage: offset +
        pageSize < sentence.length ? offset + pageSize : undefined
  }
}
```
This function returns the sentence “hello world” as an array of characters, three at a time. Retrieving the entire sentence requires four invocations:

```
> getSentenceFragment()
{ data: ['h', 'e', 'l'], nextPage: 3 }
> getSentenceFragment(3)
{ data: ['l', 'o', ' '], nextPage: 6 }
> getSentenceFragment(6)
{ data: ['w', 'o', 'r'], nextPage: 9 }
> getSentenceFragment(9)
{ data: ['l', 'd'], nextPage: undefined }
```

## The Problem

How to create a `getSentence` function that retrieves the entire sentence?

### Recursive Approach

The following function uses a recursive approach to obtain the complete sentence:

```javascript
const getSentence = (offset = 0) => {
  const fragment = getSentenceFragment(offset);
  if (fragment.nextPage) {
    return fragment.data.concat(getSentence(fragment.nextPage));
  } else {
    return fragment.data;
  }
}
```
```
> getSentence()
["h", "e", "l", "l", "o", " ", "w", "o", "r", "l", "d"]
```

### Asynchronous Recursion with Callbacks

The first step is to change the `getSentenceFragment` function so that it returns its result asynchronously.

Using a simple `setTimeout`, we can update the `getSentenceFragment` as follows:

```javascript
const getSentenceFragment = (offset, callback) => {
  const pageSize = 3
  const sentence = [...'hello world']
  setTimeout(() => callback({
    data: sentence.slice(offset, offset + pageSize),
    nextPage: offset +
        pageSize < sentence.length ? offset + pageSize : undefined
  }), 500)
}
```

This function is invoked to request a sentence fragment, and supply a callback function, which is invoked at some point in the future with the result:

```
> getSentenceFragment(0, (data) => console.log(data)))
{ data: ['h', 'e', 'l'], nextPage: 3 }
```

How do we adapt this to create a function that collects together all of the fragments and returns the complete sentence? Let’s assemble it piece-by-piece.

Firstly, the signature; rather than a synchronous return value, we need a callback:

```javascript
const getSentence = (offset, callback) => {
  ...
}
```

Next, we obtain a fragment, which is now returned via a callback:

```javascript
const getSentence = (offset, callback) => {
  getSentenceFragment(offset, (fragment) => {
    ...
  });
}
```

Next up, is the conditional logic based on whether there are more pages. Let’s take the simpler path first, where recursion terminates.

The synchronous version of this code simply returns the fragment. The asynchronous version does the same via the callback:

```javascript
const getSentence = (offset, callback) => {
  getSentenceFragment(offset, (fragment) => {
    if (fragment.nextPage) {
      ...
    } else {
      callback(fragment.data)
    }
  });
}
```

And now for the other branch, where there is another page to fetch, which is where the function is invoked recursively. Here we invoke `getSentence`, however, it returns asynchronously, so we need another `callback`, where the concatenation takes place...

```javascript
const getSentence = (offset, callback) => {
  getSentenceFragment(offset, (fragment) => {
    if (fragment.nextPage) {
      // recursively call getSentence
      getSentence(fragment.nextPage, (nextFragment) => {
        callback(fragment.data.concat(nextFragment))
      })
    } else {
      callback(fragment.data)
    }
  });
}
```

When invoked, it returns the expected output (after a few seconds):

```
> getSentence(0, (sentence) => console.log(sentence));
["h", "e", "l", "l", "o", " ", "w", "o", "r", "l", "d"]
```

The recursive version of the `getSentence` function that uses `callbacks` is not very easy to follow, with the nested callback introducing more functions and scopes.

> You can use `console.log` to follow it.


# NodeJS Challenges (Instructions) - Solving Problems  

## NodeJS's event-driven APIs, Asynchronous Recursion & Callbacks

### Objectives

- To use NodeJS's event-driven APIs.
- To use the CommonJS Module Pattern.
- To build NodeJS script with usage of `Asynchronous Recursion with Callbacks`.
- To build NodeJS script with usage of helper `reduce` method.

### Description

The company `Matrix Recursion, S.A.P.I de C.V` is interested in developing a `POC` (Proof of Concept) in which it is required to read the items of an array using `asynchronous recursion with callbacks`:

Given the following data input:

```
[
  [], 
  {
    name: "Key", 
    number: 30
  }, 
  {
    color: "Brown", 
    number: 5
  }, 
  [], 
  {
    number: 10
  }, 
  []
]

```

The following is the data output:

```
[
  { number: 10 },
  { color: 'Brown', number: 5 },
  { name: 'Key', number: 30 }
]
```

At the end, the software development team wants to get the total of the values of `​​number` property...

```
//45
```

For this, the software development team has written the modules `getList.js` and `readData.js` respectively:

- A node script that will read JSON files from `data` folder and create an array with data.

The following is the JSON files structure:

```
//data1.json
{
    "data":[
            [], 
            {
                "name": "Key", 
                "number": 30
            }, 
            {
                "color": "Brown", 
                "number": 5
            }, 
            []
        ]
}
//data2.json
{
    "data": [
                {
                    "number": 10
                }, 
                []
            ]
}
```

The following is the structure of the Array with data:

```
[
  [], 
  {
    name: "Key", 
    number: 30
  }, 
  {
    color: "Brown", 
    number: 5
  }, 
  [], 
  {
    number: 10
  }, 
  []
]

```

- A NodeJS module `getList` that should read the items from an array with usage of `Asynchronous Recursion with Callbacks`:

How do the software development team adapt some pieces of code to create a module that collects together all of the items and returns the array only with objects to get sum of the values of `number` properties?

The following function is invoked to request a fragment of an array, and supply a callback function, which is invoked at some point in the future with the result:

```javascript
const getListFragment = (offset, data, callback) => {
    const listSize = 0
    const list = data   //[[], {name: "Key", number: 30}, {color: "Brown", number: 5}, [], {number: 10}, []]
    setTimeout(() => callback({
      data: list[offset + listSize],
      nextItem: offset +
          listSize < list.length - 1  ? ++offset : undefined
    }), 500)
  }
```

Rather than a synchronous return value, it needs a callback:

```javascript
const getList = (offset, data, callback) => {
  ...
}
```

Next, it obtains a fragment, which is now returned via a callback:

```javascript
const getList = (offset, data, callback) => {
    getListFragment(offset, data, (fragment) => {    
        ...
  })
}
```

The asynchronous code version gives each item via the callback:

```javascript
const getList = (offset, data, callback) => {
    getListFragment(offset, data, (fragment) => {
      if (fragment.nextItem) {
          ...
        } else {
          callback(null, dataList)
        }
        
    })
}
```

The problem:

1. Where goes the function invoked recursively to get the array with objects? So... the sum of values from `number` properties takes place.

2. What else is necessary to get the array with objects?

When invoked, it returns the expected output (after a few seconds):

```
>getList(0, data, (err, value) => {
    if (err) throw err
    console.log(value)
})
[
  { number: 10 },
  { color: 'Brown', number: 5 },
  { name: 'Key', number: 30 }
]
```

Check that the Proof of Concept (POC) works correctly and returns the required result:

```
Output: 45
```

### Constrainsts

- It´s a must to use the `CommonJS Module` Pattern.
- It´s a must to use `Asynchronous Recursion with Callback Pattern`.
- It´s important to use `Asynchronous operations` with NodeJS's event-driven APIs. 
  Only when it's necessary to use `Synchronous operations`. 
- It's important to use `callbacks`.
- Best practices to avoid `callback hell`.
- It's important to handle errors.
- Only use `reduce` helper method to sum values.

### Sprints

1) Create project and install dependencies: `npm i`.
2) Test your module `readData.js`: `npm test`.
3) Get new list with objects.
4) Test your module `getList.js`: `npm test`.
5) Get sum of values.
6) Test your module `getSum.js`: `npm test`.
7) Test your project: `npm test`.

## Deliverables

- When you run 'npm start'...

You'll have in terminal…

```
$ npm start

List of Objects...
[
  { number: 10 },
  { color: 'Brown', number: 5 },
  { name: 'Key', number: 30 }   
]
Output: 45
```

- When you run 'npm test'...

You'll have in terminal…

```
$ npm test

> jest

 PASS  tests/recursion.test.js (14.181 s)
  readDataList
    √ it should be data an instance of array (32 ms)
    √ it should read data length from array (2 ms)
    √ it should read items from array (2 ms)
  getList
    √ it should be data an instance of array (3059 ms)
    √ it should read data length from array (3022 ms)
    √ it should read number of key/properties from array (3050 ms)
    √ it should read key and properties from array (3044 ms)
  sum function
    √ should sum values of property "number" (3 ms)

Test Suites: 1 passed, 1 total
Tests:       8 passed, 8 total
Snapshots:   0 total
Time:        14.839 s
Ran all test suites.
```


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:

1. [Adapted from Asynchronous Recursion: https://blog.scottlogic.com/2017/09/14/asynchronous-recursion.html.](https://blog.scottlogic.com/2017/09/14/asynchronous-recursion.html)

