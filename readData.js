//import fs
const fs = require('fs')
const path = require('path')

//readData()
//get each data from files 
const readData = (err, dir, files, callback) => {
    if (err) throw err

    let dataList = []
    let pending = files.length

    //read each file
    files.forEach(file => {
        const fullPath = path.join(dir, file)
        fs.readFile(fullPath, 'utf8', (err, data) => {
            if (err) callback(err)
            //read each data to get single items
            JSON.parse(data).data.forEach(item => {
                dataList.push(item)
            })
            //when status is true it gets array with items
            if (!--pending) callback(null, dataList)
        })
    })
}

const readDirectory = (dir, readData, callback) => {
    fs.readdir(dir, (err, files) => {
        if (err) readData(err)
        readData(null, dir, files, callback)
    })
}

const readDataList = (filePath, callback) => {
    readDirectory(path.join(__dirname, filePath), readData, callback)
}


module.exports = {
    readDataList
}
