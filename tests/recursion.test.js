const { getList } = require('../getList')
const { readDataList } = require('../readData')
const { sum } = require('../getSum')

//test readData module
describe('readDataList', () => {
    //files directory
    const filePath = '/data'

    test('it should be data an instance of array', done => {
        readDataList(filePath, (err, data) => {
            expect( data instanceof Array ).toBeTruthy()
            done()
        })
    })

    test('it should read data length from array', done => {
        readDataList(filePath, (err, data) => {
            expect( data.length ).toBe(6)
            done()
        })
    })

    test('it should read items from array', done => {
        readDataList(filePath, (err, data) => {
            const listOfObjects = [...new Set(data.map(value => Object.entries(value)).flat(Infinity))]
            const output = 0
            
            expect(listOfObjects.indexOf('number')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('color')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Brown')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('name')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Key')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(10)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(5)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(30)).toBeGreaterThanOrEqual(output)
            done()
        })
    })

})

//test getList module
describe('getList', () => {
    const offset = 0
    //data input to test 'getList()'
    const dataInput = [
                        [], 
                        {
                            name: "Key", 
                            number: 30
                        }, 
                        {
                            color: "Brown", 
                            number: 5
                        }, 
                        [], 
                        {
                            number: 10
                        }, 
                        []
                    ]

    test('it should be data an instance of array', done => {
        getList(offset, dataInput, (err, data) => {
            expect( data instanceof Array ).toBeTruthy()
            done()
        })
    })

    test('it should read data length from array', done => {
        getList(offset, dataInput, (err, data) => {
            expect( data.length ).toBe(3)
            done()
        })
    })

    test('it should read number of key/properties from array', done => {
        getList(offset, dataInput, (err, data) => {
            const listOfObjects = [...new Set(data.map(value => Object.entries(value)).flat(Infinity))]
            expect(listOfObjects.length).toBe(8)
            done()
        })
    })

    test('it should read key and properties from array', done => {
        getList(offset, dataInput, (err, data) => {
            const listOfObjects = [...new Set(data.map(value => Object.entries(value)).flat(Infinity))]
            const output = 0
            
            expect(listOfObjects.indexOf('number')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('color')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Brown')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('name')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Key')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(10)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(5)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(30)).toBeGreaterThanOrEqual(output)
            done()
        })
    })
})

//test getSum module
describe('sum function', () => {
    const input = [
                    { number: 10 },
                    { color: 'Brown', number: 5 },
                    { name: 'Key', number: 30 }
                ],
          output = 45
    test('should sum values of property "number"', ()=> {
        expect( sum(input) ).toBe(output)
    })
})