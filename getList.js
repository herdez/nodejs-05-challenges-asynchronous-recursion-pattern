//getListFragment()
const getListFragment = (offset, data, callback) => {
    const listSize = 0;
    const list = data   //[[], {name: "Key", number: 30}, {color: "Brown", number: 5}, [], {number: 10}, []];
    setTimeout(() => callback({
      data: list[offset + listSize],
      nextItem: offset +
          listSize < list.length - 1  ? ++offset : undefined
    }), 500);
  };

//getList()
const getList = (offset, data, callback) => {
    getListFragment(offset, data, (fragment) => {
      if (fragment.nextItem) {
        //...
      } else {
        callback(null, dataList)
      }
        
    })
}

module.exports = {
    getList
}